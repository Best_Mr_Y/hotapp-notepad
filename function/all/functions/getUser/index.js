const cloud = require("wx-server-sdk")
cloud.init()

exports.main = (event,context) =>{
    let {OPENID,APPID} = cloud.getWXContext()
    return {
        OPENID
    }
}