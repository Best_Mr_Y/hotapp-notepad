import { Component } from 'react'
import './app.scss'

class App extends Component {
  state = {
    openid: ""
  }
  componentDidMount() {
    // 云开发初始化   
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        // env 参数说明：
        //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
        env: 'myapp-7gzkl8ud3de4e09b',
        // traceUser: true,
      })
    }

    wx.cloud.callFunction({
      name: "getOpenid",
      success: res => {
        // console.log(res);
        // console.log(res.result.openid)
        this.setState({

          openid: res.result.openid
        })
        console.log(this.state.openid)
      },
      fail: err => {
        console.error(err);
      }
    })
  }

  componentDidHide() { }

  componentDidCatchError() { }


  // this.props.children 是将要会渲染的页面
  render() {
    return this.props.children
  }
}

export default App
