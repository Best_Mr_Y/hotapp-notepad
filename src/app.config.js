export default {
  pages: [
    'pages/newnote/newnote',
    'pages/person/person',
    'pages/index/index',
    'pages/about/about',
    'pages/complaints/complaints',
    'pages/data/data',
    'pages/diary/diary',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  "tabBar": {
    "color": "#333",
    "selectedColor": "#6ff",
    "backgroundColor": "#fff",
    "borderStyle": "black",
    "list": [
      {
        "pagePath": "pages/newnote/newnote",
        "text": "写笔记",
        "iconPath": "icon/add.png",
        "selectedIconPath": "icon/add-c.png"
      },
      {
        "pagePath": "pages/data/data",
        "text": "笔记",
        "iconPath": "icon/list.png",
        "selectedIconPath": "icon/list-c.png"
      },
      {
        "pagePath": "pages/person/person",
        "text": "我的",
        "iconPath": "icon/mine.png",
        "selectedIconPath": "icon/mine-c.png"
      },
    ]
  },
}
