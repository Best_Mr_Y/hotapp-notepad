import { Component } from 'react'

import { View, Text } from '@tarojs/components'
import { AtTextarea,AtButton } from 'taro-ui'
import './complaints.scss'
import "taro-ui/dist/style/components/textarea.scss"
import "taro-ui/dist/style/components/button.scss"

export default class Complaints extends Component {
    state={
        content:''
    }
    handleChange(value){
        this.setState({
            content:value
        })
    }
    submitComplaints=()=>{
        var content = this.state.content
        wx.showLoading({
            title:'加载中...'
        })
        const db = wx.cloud.database()
        db.collection('notepad_suggest')
        .add({data:{content:content}})
        .then(res=>{
            this.setState({
                content:""
            })
            wx.hideLoading()
            wx.showToast({
                title:'成功'
            })
        })
    }
    render() {
        return (
            <View>
                <View className="complaints-box">
                    <View>
                        <AtTextarea
                        value={this.state.content}
                        onChange={this.handleChange.bind(this)}
                        maxLength={150}
                        placeholder='你的问题是...'
                        height="200"
                        ></AtTextarea>
                        <View className="submit">
                            <AtButton type='primary' onClick={this.submitComplaints}>提交</AtButton>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
