import { Component } from 'react'
import { View, Text, Button, Input } from '@tarojs/components'
import { AtButton } from 'taro-ui'
import "taro-ui/dist/style/components/button.scss";
import "taro-ui/dist/style/components/loading.scss";

import "taro-ui/dist/style/components/button.scss" // 按需引入
import './index.scss'
import { random } from 'lodash'

export default class Index extends Component {
  state = {
    heights: '',
    list:[]
  }
  changediary=(l)=>{
    wx.navigateTo({
      url:'../diary/diary?id='+ l._id
    })
    console.log(l);
  }
  componentWillMount() {
    const db = wx.cloud.database()
    db.collection('notepad_note').get()
    .then(res=>{
      // console.log(res);
      this.setState({
        list:res.data
      })
      // console.log(this.state.list);
      wx.hideLoading({
        success: (res) => {},
      })
      wx.showToast({
        title: 'success',
      })
    })
  }

  componentDidMount() {

    

   }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
  
    return (
      <View className='index'>
        <view className="search">
          <Input className="searchInp" type='text' placeholder='搜索日记' />
          <Button size='mini' type='warn'>搜索</Button>
        </view>
        <view className="container">
      
        {
          this.state.list.map((l,i)=>{
          return (
            <view   onClick={()=>this.changediary(l)} style={{height:"240px"}}  className="content" hover-class='bj' hover-start-time>
            <view  className="box">
              <view className="boxTitle">{l.title} </view>
          <view className="boxContent">{l.context}</view>
              <view className="boxTime"> {l.time}  </view>
           
            </view>
          </view>
          )
          })
        }  
    
          {/* <view  style={{height:"240px"}}  className="content" hover-class='bj' hover-start-time>
     

            <view  className="box">
              <view className="boxTitle">今天很开心</view>
              <view className="boxContent">我捡到了一块钱红红火火恍恍惚惚或或或或或或或或或或或或哈</view>
              <view className="boxTime">2021/4/15</view>
              <Button type='secondary' onClick={this.changediary}> 修改日记 </Button>
            </view>
          </view>
          <view  style={{height:"240px"}}  className="content" hover-class='bj' hover-start-time>
            <view  className="box">
              <view className="boxTitle">今天很开心</view>
              <view className="boxContent">我捡到了一块钱红红火火恍恍惚惚或或或或或或或或或或或或哈</view>
              <view className="boxTime">2021/4/15</view>
              <Button type='default'> 修改日记 </Button>
            </view>
          </view>
          <view  style={{height:"240px"}}  className="content" hover-class='bj' hover-start-time>
            <view  className="box">
              <view className="boxTitle">今天很开心</view>
              <view className="boxContent">我捡到了一块钱红红火火恍恍惚惚或或或或或或或或或或或或哈</view>
              <view className="boxTime">2021/4/15</view>
              <Button type='default'> 修改日记 </Button>
            </view>
          </view>
          <view  style={{height:"240px"}}  className="content" hover-class='bj' hover-start-time>
            <view  className="box">
              <view className="boxTitle">今天很开心</view>
              <view className="boxContent">我捡到了一块钱红红火火恍恍惚惚或或或或或或或或或或或或哈</view>
              <view className="boxTime">2021/4/15</view>
              <Button type='default'> 修改日记 </Button>
            </view>
          </view> */}
        </view>

      </View>

    )
  }
}
