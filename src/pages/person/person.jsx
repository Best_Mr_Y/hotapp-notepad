import { Component } from 'react'

import { View, Text } from '@tarojs/components'
import { AtButton, AtList, AtListItem, AtIcon } from 'taro-ui'

import "taro-ui/dist/style/components/button.scss" // 按需引入
import "taro-ui/dist/style/components/list.scss"
import "taro-ui/dist/style/components/icon.scss"
import './person.scss'

export default class Person extends Component {

  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }
  shareClick() {
    console.log(111);
  }
  publicClick() {
    wx.navigateTo({
      url: '../publicAH/publicAH',
    })
  }
  aboutMeClick() {
    wx.navigateTo({
      url: '../about/about',
    })
    // wx.switchTab({ 
    //   url: '../about/about',
    // })
  }
  complaintsClick() {
    wx.navigateTo({
      url: '../complaints/complaints',
    })
  }
  render() {
    return (
      <View className='person'>
        <View className='avatar-box'>
          <View className="imgBox">
            <open-data type="userAvatarUrl" className="avatar-imgBox"></open-data>
          </View>
          <open-data type="userNickName" className="userinfo-username"></open-data>
        </View>
        <View className='list'>
          <AtList>
            {/* <AtListItem 
                title='分享小程序' 
                onClick={this.shareClick} 
                arrow='right' 
                iconInfo={{ size: 25, color: '#78A4FA', value: 'external-link', }}
                />
                <AtListItem 
                title='啊哈公众号' 
                onClick={this.publicClick} 
                arrow='right' 
                iconInfo={{ size: 25, color: '#78A4FA', value: 'bookmark', }}
                /> */}
            <AtListItem
              title='关于啊哈云笔记'
              onClick={this.aboutMeClick}
              arrow='right'
              iconInfo={{ size: 25, color: '#78A4FA', value: 'help', }}
            />
            <AtListItem title='' />
            <AtListItem
              title='反馈与投诉'
              onClick={this.complaintsClick}
              arrow='right'
              iconInfo={{ size: 25, color: '#FF4949', value: 'edit', }}
            />
          </AtList>
        </View>
      </View>
    )
  }
}
