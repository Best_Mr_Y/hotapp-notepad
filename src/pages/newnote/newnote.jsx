import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import { AtButton, AtInput, AtForm, AtTextarea } from 'taro-ui'

import "taro-ui/dist/style/components/button.scss" // 按需引入
import "taro-ui/dist/style/components/input.scss";
import "taro-ui/dist/style/components/icon.scss";
import "taro-ui/dist/style/components/textarea.scss";
import "taro-ui/dist/style/components/form.scss";
import './newnote.scss'
var db = wx.cloud.database()
var openid = null;
export default class Newnote extends Component {

  state = {
    title: "",
    context: ""
  }
  componentWillMount() {

  }
  componentDidMount() {
  }

  componentWillUnmount() { }


  componentDidShow() { }

  componentDidHide() { }

  changeTitle(e) {
    this.setState({
      title: e
    })
  }
  changeContext = (e) => {
    this.setState({
      context: e
    })
  }
  onSubmit = () => {

    var app = getApp();
    openid = app.$app.state.openid
    const { title, context } = this.state
    if (title || context) {

      const noteid = openid + "_" + (new Date()).toJSON
      wx.showLoading({
        title: 'Loading',
      })
      db.collection('notepad_note')
        .add({
          data: {
            openid,
            noteid,
            title,
            context,
            time: (new Date()).toLocaleString()
          },
          success:  (res)=> {
            console.log(res);
            wx.hideLoading()
            wx.showToast({
              title: "已保存"
            })
            this.setState({
              title:"",
              context:""
            })
            wx.switchTab({
              url:'../data/data'
            })

          }
        })
    } else {
      wx.showToast({
        title: "内容为空",
        icon: "error"
      })
    }
  }
  render() {
    return (
      <AtForm
        className='newnote'

      >
        <AtInput
          name='title'
          type='text'
          placeholder='标题'
          value={this.state.title}
          onChange={this.changeTitle.bind(this)}
        />
        <AtTextarea
          value={this.state.context}
          placeholder='写笔记...'
          showConfirmBar
          height={400}
          onChange={this.changeContext.bind(this)}

        />
        <AtButton onClick={this.onSubmit} className="sendBtn" type='secondary' circle={true}>保存</AtButton>
      </AtForm>
    )
  }
}
