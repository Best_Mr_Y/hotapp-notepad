import { Component } from 'react'

import { View, Text } from '@tarojs/components'
import './about.scss'


export default class About extends Component {
    componentDidMount(){
        wx.setBackgroundColor({
            backgroundColor: '#8fd3f4', // 窗口的背景色为白色
        })
    }
    
    render() {
        return (
            <View className="about-me">
                <View className="about-txt">
                    <Text>记录，成为更好的自己</Text>
                </View>
                <View className="about-foot">
                    <Text style={{fontSize:18,lineHeight:'32px'}}>访问官网，获取并体验更多能力</Text>
                    <Text>http://calabashbaby.cn/build</Text>
                </View>
            </View>
        )
    }
}
