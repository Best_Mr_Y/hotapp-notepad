import { Component } from 'react'
import { View, Text, Form, Switch, Input, Button, Textarea } from '@tarojs/components'
import { AtButton, AtInput, AtForm, AtTextarea } from 'taro-ui'

import "taro-ui/dist/style/components/button.scss" // 按需引入
import "taro-ui/dist/style/components/input.scss";
import "taro-ui/dist/style/components/icon.scss";
import "taro-ui/dist/style/components/textarea.scss";
import "taro-ui/dist/style/components/form.scss";

import './index.scss'
import { random } from 'lodash'
var openid = null;
var db = wx.cloud.database()

export default class Index extends Component {
  state = {
    heights: '',
    list: '',
    title: '',
    context: '',
    _id: ''
  }
  componentWillMount() {

  }

  componentDidMount() {

    const query = this.props.tid
    console.log(query);
    const _id = query.split('=')[1]
    this.setState({
      _id: _id
    })
    console.log(_id);

    const db = wx.cloud.database()
    db.collection('notepad_note').where({ _id: _id }).get()
      .then(res => {
        console.log(res);
        this.setState({
          list: res.data[0]

        })
        console.log(this.state.list);

      })
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }
  formSubmit = (v) => {
    console.log(v.detail.value);
    const data = v.detail.value
    var times = new Date()
    console.log(times);
    wx.showLoading({
      title: 'Loading',
    })
    db.collection("notepad_note")
      .where({
        _id: this.state._id
      })
      .update({
        time: 132132132,
        data,
      })
      .then(res => {
        console.log(res)
        wx.hideLoading({
          success: (res) => { },
        })
        wx.showToast({ title: "修改成功" })
        wx.navigateTo({
          url: '../data/data'
        })
      })
  }
  render() {

    return (
      <Form onSubmit={this.formSubmit} onReset={this.formReset} >
        <Input
          name='title'

          className="titleInp"
          type='text'
          placeholder='标题'
          value={this.state.list.title}
          autoFocus={true}
          showConfirmBar

        />
        <Textarea
          className="abac"
          name="context"
          value={this.state.list.context}
          placeholder='写笔记...'
          showConfirmBar
          height={400}
        />
        <Button formType="submit" className='btn-max-w' plain >修改</Button>
      </Form>
    )
  }
}
